class AbstractStorage {
    static storage = null;

    constructor() {
        if (!AbstractStorage.storage) {
            AbstractStorage.storage = this;
        }

        return AbstractStorage.storage;
    }

    /**
     * Load folders to the FoldersResource
     *
     * @returns {Folder[]}
     */
    loadFolders() {
        return [];
    }

    /**
     * Save folders to the browser localStorage
     *
     * @param {Folder[]} folders
     */
    saveFolders(folders) {
    }
}