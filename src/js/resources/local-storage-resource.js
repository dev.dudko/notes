class LocalStorageResource extends AbstractStorage {
    loadFolders() {
        let folders = JSON.parse(localStorage.getItem('folders'));

        if (!folders) {
            return [];
        }

        let result = [];

        for (let folder of folders) {
            let localFolder = new Folder();
            localFolder.title = folder._title;

            for (let note of folder._notes) {
                let localNote = new Note();
                localNote.text = note._text;
                localNote.creationDate = note._creationDate;
                localFolder.notes.push(localNote);
            }

            result.push(localFolder);
        }

        return result;
    }

    saveFolders(folders) {
        localStorage.setItem('folders', JSON.stringify(folders));
    }
}