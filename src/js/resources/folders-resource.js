class FoldersResource {
    static foldersResource = null;

    /**
     * @param {AbstractStorage} storage
     * @returns {this}
     */
    constructor(storage) {
        if (!FoldersResource.foldersResource) {
            FoldersResource.foldersResource = this;
            this._storage = storage;
            this._folders = this._storage.loadFolders();
        }

        return FoldersResource.foldersResource;
    }

    /**
     * @returns {AbstractStorage}
     */
    get storage() {
        return this._storage;
    }

    /**
     * @returns {Folder[]}
     */
    get folders() {
        return this._folders;
    }

    /**
     * @param {Folder} newFolder
     */
    addNewFolder(newFolder) {
        this.folders.push(newFolder);
        this.storage.saveFolders(this.folders);
    }

    /**
     * @param {string} folderId
     */
    removeFolder(folderId) {
        let removeIndex = this.folders.findIndex(p => p.id === folderId);
        this.folders.splice(removeIndex, 1);
        this.storage.saveFolders(this.folders);
    }

    /**
     * @param {string} folderId
     * @param {string} newFolderTitle
     */
    editFolderName(folderId, newFolderTitle) {
        let folder = this.folders.find(p => p.id === folderId);
        folder.title = newFolderTitle;
        this.storage.saveFolders(this.folders);
    }

    /**
     * @param {string} folderId
     * @returns {Folder}
     */
    getFolderById(folderId) {
        return this.folders.find(p => p.id === folderId);
    }

    /**
     * @param {string} folderId
     * @returns {Note[]}
     */
    getNotes(folderId) {
        return this.getFolderById(folderId).notes;
    }

    /**
     * @param {string} folderId
     * @param {Note} newNote
     */
    addNewNoteInFolder(folderId, newNote) {
        let folder = this.folders.find(p => p.id === folderId);
        folder.addNote(newNote);
        this.storage.saveFolders(this.folders);
    }

    /**
     * @param {string} folderId
     * @param {Note} note
     */
    editNoteInFolder(folderId, note) {
        let folder = this.folders.find(p => p.id === folderId);

        if (!folder) {
            return;
        }

        let folderNote = folder.getNoteById(note.id);
        folderNote.editText(note.text)
        this.storage.saveFolders(this.folders);
    }

    /**
     * @param {string} folderId
     * @param {Note} note
     */
    remoteNoteFromFolder(folderId, note) {
        let folder = this.folders.find(p => p.id === folderId);
        folder.removeNote(note.id);
        this.storage.saveFolders(this.folders);
    }
}