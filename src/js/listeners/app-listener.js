class AppListener {
    _folderAdd  = document.querySelector(`.${FOLDERS_ADD_BTN_KEY}`);
    _folderList = document.querySelector(`.${FOLDERS_LIST_KEY}`);
    _notes  = document.querySelector(`.${NOTES_KEY}`);
    _noteField  = document.querySelector(`.${NOTE_FIELD_KEY}`);

    run() {
        this.folderResource = new FoldersResource(new LocalStorageResource());
        this.foldersView = new FoldersView();
        this.notesView = new NotesView();
        this.noteView = new NoteView();

        this.observer = new Observer();
        this.observer.subscribe(this.foldersView.renderAllFolders.bind(this.foldersView));
        this.observer.subscribe(this.notesView.showNotes.bind(this.notesView));

        this.foldersView.browserFolderList = this._folderList;
        this.foldersView.renderAllFolders();
        this.foldersView.observer = this.observer;

        this.notesView.notesContent = this._notes;

        this.noteView.noteField = this._noteField;
        this.noteView.observer = this.observer;

        this.bodyListener.call(this);
        this.addNewFolderListener.call(this);
        this.dropdownToggleListener.call(this);
        this.showNotesListener.call(this);
    }

    addNewFolderListener() {
        this._folderAdd.addEventListener('click', () => {
            this.foldersView.hideFoldersActive();
            this.foldersView.renderNewFolder();
            this.notesView.closeNotes();
        });
    }

    dropdownToggleListener() {
        this._folderList.addEventListener('click', (e) => {
            this.foldersView.manageFolderSettings(e);
        });
    }

    showNotesListener() {
        this._folderList.addEventListener('click', (e) => {
            const folder = e.target.closest(`.${FOLDER_KEY}`);

            if (!folder) {
                return;
            }

            this.foldersView.hideFoldersActive();
            folder.classList.add(`${FOLDER_ACTIVE_KEY}`);

            const folderId = folder.dataset.folderId;
            const curFolder = this.folderResource.getFolderById(folderId);

            this.noteView.curFolder = curFolder
            this.notesView.curFolder = curFolder;

            this.notesView.closeNotes();
            this.notesView.showNotes();
        })
    }

    bodyListener() {
        document.body.addEventListener('click', (e) => {
            this.folderSettingsToggleListener(e);
            this.noteAddListener(e);
            this.noteRemoveListener(e);
            this.noteHeaderActiveListener(e);
        });
    }

    folderSettingsToggleListener(e) {
        if (e.target.closest('.' + SETTING_BTN_KEY)) {
            return;
        }

        this.foldersView.hideActiveSettings();
    }

    noteAddListener(e) {
        if (!e.target.classList.contains(NOTE_ADD_KEY)) {
            return;
        }

        this.noteView.addNewNote();
        this.observer.notify();
    }

    noteRemoveListener(e) {
        const removeBtn = e.target.closest(`.${NOTE_BTN_REMOVE_KEY}`);

        if (!removeBtn) {
            return;
        }

        const noteItem = removeBtn.parentNode;
        const noteId = noteItem.dataset.noteId;
        this.notesView.closeNotes();

        const folderDiv = document.querySelector(`.${FOLDER_ACTIVE_KEY}`);
        (new FoldersView()).activeFolderId = folderDiv.dataset.folderId;
        (new NotesView()).activeNoteId = '';

        let curFolder = this.folderResource.getFolderById(this.notesView.curFolder.id);
        let note = curFolder.getNoteById(noteId);
        this.folderResource.remoteNoteFromFolder(curFolder.id, note);

        this.observer.notify();
    }

    noteHeaderActiveListener(e) {
        const note = e.target.closest(`.${NOTE_KEY}`);

        if (!note) {
            return;
        }

        const noteRemoveBtn = e.target.closest(`.${NOTE_BTN_REMOVE_KEY}`);
        if (noteRemoveBtn) {
            return;
        }

        this.notesView.hideNotesActive();
        note.classList.add(`${NOTE_ACTIVE_KEY}`)
        this.noteView.showNote(note.dataset.noteId);
    }
}