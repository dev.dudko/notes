class Folder {
    constructor() {
        this._notes = [];
        this._title = FOLDER_DEFAULT_NAME;
        this._id = CryptoJS.SHA256(Date.now().toString()).toString();
    }

    /**
     * @returns {string}
     */
    get title() {
        return this._title;
    }

    /**
     * @param {string} title
     */
    set title(title) {
        this._title = title;
    }

    /**
     * @returns {Note[]}
     */
    get notes() {
        return this._notes;
    }

    /**
     * @param {Note[]} notes
     */
    set notes(notes) {
        this._notes = notes;
    }

    /**
     * @returns {string}
     */
    get id() {
        return this._id;
    }

    /**
     * @param {Note} note
     */
    addNote(note) {
        this.notes.push(note);
    }

    /**
     * @param {string} noteId
     */
    removeNote(noteId) {
        let removeIndex = this.notes.findIndex(p => p._id === noteId);
        this.notes.splice(removeIndex, 1);
    }

    /**
     * @param {string} noteId
     * @returns {Note|undefined}
     */
    getNoteById(noteId) {
        return this.notes.find(p => p.id === noteId);
    }
}