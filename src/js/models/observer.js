class Observer {

    /**
     * Create empty array of observers
     */
    constructor() {
        this.observers = [];
    }

    /**
     * @param {Function} observer
     */
    subscribe(observer) {
        if (observer && !this.observers.includes(observer)) {
            this.observers.push(observer);
        }
    }

    /**
     * Call all observers to execute
     */
    notify() {
        this.observers.forEach(observer => observer());
    }
}