class Note {
    constructor() {
        this._text = '';
        this._title = '';
        this._creationDate = Date.now();
        this._id = CryptoJS.SHA256(Date.now().toString()).toString();
    }

    /**
     * Return the note name
     * The name is the first 60 characters or all characters up to line feed, whichever comes first
     * If the note is empty, then its name will be 'Unnamed'
     *
     * @returns {string}
     */
    get title() {
        let title = (this._text.split('\n', 1))[0]
            .trim()
            .slice(0, MAX_NOTE_TITLE_LENGTH);

        title = title.length < MAX_NOTE_TITLE_LENGTH ? title : title + ELLIPSIS;
        return title.length === 0 ? NOTE_DEFAULT_NAME : title;
    }

    /**
     * @returns {Date}
     */
    get creationDate() {
        return this._creationDate;
    }

    /**
     * @param {Date} date
     */
    set creationDate(date) {
        this._creationDate = date;
    }

    /**
     * @returns {string}
     */
    get text() {
        return this._text;
    }

    /**
     * @param {string} text
     */
    set text(text) {
        this._text = text;
    }

    /**
     * @returns {string}
     */
    get id() {
        return this._id;
    }

    /**
     * @param {string} newText
     */
    editText(newText) {
        this.text = newText;
    }
}