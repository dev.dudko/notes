class Formatter {
    static formater = null;

    constructor() {
        if (!Formatter.formater) {
            Formatter.formater = this;
        }

        return Formatter.formater;
    }

    /**
     * Format date to the next form - eg.: 1st September, 2021 16:22:00
     *
     * @param {Date} date
     * @returns {string}
     */
    formatDate(date) {
        const now     = new Date(date);
        const day     = now.getDate();
        const month   = now.getMonth();
        const hours   = this.addLeadZero(now.getHours());
        const minutes = this.addLeadZero(now.getMinutes());
        const seconds = this.addLeadZero(now.getSeconds());

        const monthName = [
            'January', 'February', 'March',
            'April', 'May', 'June',
            'July', 'August', 'September',
            'October', 'November', 'December'
        ];

        return `${day}${this.chooseDayEnd(day)} ${monthName[month]}, ${now.getFullYear()}, ${hours}:${minutes}:${seconds}`;
    }

    /**
     * Returns the string ending of the day of the week
     *
     * @param {int} day
     * @returns {string}
     */
    chooseDayEnd(day) {
        switch (day) {
            case 1:
            case 21:
            case 31:
                return  'st';
            case 2:
            case 22:
                return 'nd';
            case 3:
            case 23:
                return  'rd';
            default:
                return 'th';
        }
    }

    /**
     * Converts a number to two-character format
     * Adds 0 to a one-character number
     *
     * @param {int} time
     * @returns {string}
     */
    addLeadZero(time) {
        return time < 10 ? `0${time}` : time;
    }

}