class NotesView {
    static notesView = null;

    constructor() {
        if (!NotesView.notesView) {
            NotesView.notesView = this;
            this._activeNoteId = '';
            this._curFolder = null;
            this._notesContent = null;
        }

        return NotesView.notesView;
    }

    /**
     * @returns {string}
     */
    get activeNoteId() {
        return this._activeNoteId;
    }

    /**
     * @param {string} activeNoteId
     */
    set activeNoteId(activeNoteId) {
        this._activeNoteId = activeNoteId;
    }

    /**
     * @returns {Folder}
     */
    get curFolder() {
        return this._curFolder;
    }

    /**
     * @param {Folder} curFolder
     */
    set curFolder(curFolder) {
        this._curFolder = curFolder;
    }

    /**
     * @returns {HTMLElement}
     */
    get notesContent() {
        return this._notesContent;
    }

    /**
     * @param {HTMLElement} notesList
     */
    set notesContent(notesList) {
        this._notesContent = notesList;
    }

    /**
     * Show note list for user-selected folder in the browser view
     */
    showNotes() {
        const noteList = this.notesContent.querySelector(`.${NOTES_LIST_KEY}`);
        const noteBtn = this.notesContent.querySelector(`.${NOTES_BTN_KEY}`);
        noteList.innerHTML = '';
        noteBtn.innerHTML = '';

        if (!this.curFolder) {
            return;
        }

        this.insertNotesInHTML(noteList);
        this.insertAddNoteBtnInHTML(noteBtn);
    }

    /**
     * @param {HTMLUListElement} noteList
     */
    insertNotesInHTML(noteList) {
        this.curFolder.notes.map(note => {
            noteList.insertAdjacentHTML('beforeend',
                `<li class="note ${this.activeNoteId === note.id ? NOTE_ACTIVE_KEY : ''}"
                          data-note-id=${note.id}>
                    <div class="note__header">
                        <p class="note__title">${note.title}</p>
                        <p class="note__date">${(new Formatter()).formatDate(note.creationDate)}</p>
                    </div>
                    <button class="note__btn-remove setting__btn">
                        <img
                            class="btn-setting__img"
                            src="img/svg/bin.svg"
                            width="16"
                            height="16"
                            alt="Open settings dropdown menu">
                    </button>
                </li>`
            )
        });
    }

    /**
     * @param {HTMLDivElement} noteBtn
     */
    insertAddNoteBtnInHTML(noteBtn) {
        noteBtn.insertAdjacentHTML('beforeend',
            `<button class="btn btn_primary note__add-btn">Create new note</button>`
        );
    }

    /**
     * Hide note list in the browser view
     */
    closeNotes() {
        const noteList = this.notesContent.querySelector(`.${NOTES_LIST_KEY}`);
        const noteBtn = this.notesContent.querySelector(`.${NOTES_BTN_KEY}`);
        const noteContent = document.querySelector(`.${NOTE_FIELD_CONTENT_KEY}`);
        this.activeNoteId = '';

        noteList.innerHTML = '';
        noteBtn.innerHTML = '';
        noteContent.innerHTML = '';
    }

    /**
     * Hide active folder state
     */
    hideNotesActive() {
        const notes = document.querySelectorAll(`.${NOTE_KEY}`);
        notes.forEach(folder => {
            folder.classList.remove(`${NOTE_ACTIVE_KEY}`);
        });
    }
}