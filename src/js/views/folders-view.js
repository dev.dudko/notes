class FoldersView {
    static foldersView = null;

    constructor() {
        if (!FoldersView.foldersView) {
            FoldersView.foldersView = this;
            this._observer = null;
            this._activeFolderId = '';
            this._browserFolderList = null;
        }

        return FoldersView.foldersView;
    }

    /**
     * @returns {HTMLUListElement}
     */
    get browserFolderList() {
        return this._browserFolderList;
    }

    /**
     * @param {HTMLUListElement} folderList
     */
    set browserFolderList(folderList) {
        this._browserFolderList = folderList;
    }

    /**
     * @returns {Observer}
     */
    get observer() {
        return this._observer;
    }

    /**
     * @param {Observer} observer
     */
    set observer(observer) {
        this._observer = observer;
    }

    /**
     * @returns {string}
     */
    get activeFolderId() {
        return this._activeFolderId;
    }

    /**
     * @param {string} activeFolderId
     */
    set activeFolderId(activeFolderId) {
        this._activeFolderId = activeFolderId;
    }

    /**
     * Render all folders in the browser view
     */
    renderAllFolders() {
        this.browserFolderList.innerHTML = '';

        (new FoldersResource()).folders.map(folder => {
            this.insertFolderInHTML(folder, this.browserFolderList, 'beforeend');
        })
    }

    /**
     * Render new folder in the browser view
     */
    renderNewFolder() {
        if (!this.browserFolderList) {
            return;
        }

        let listItem = document.createElement('li');
        let input = document.createElement('input');

        input.value = FOLDER_DEFAULT_NAME;
        input.classList.add(FOLDER_INPUT_KEY);
        listItem.classList.add(FOLDER_KEY);
        listItem.append(input);

        this.browserFolderList.append(listItem);
        input.focus();
        this.newFolderBlurListener(input);
    }

    /**
     * @param {Folder} folder - folder for insert
     * @param {HTMLElement} elem - element where insert
     * @param {string} position - place for insert
     */
    insertFolderInHTML(folder, elem, position) {
        elem.insertAdjacentHTML(position,
            `<li class="folder ${this.activeFolderId === folder.id ? FOLDER_ACTIVE_KEY : ''}" 
                      data-folder-id=${folder.id}
                  >
                    <p class="folder__info">
                        <span class="folder__notes-count">${folder.notes.length}</span>
                        <span class="folder__name">${folder.title}</span>
                    </p>
                    <div class="folder__setting setting">
                    <button class="folder__btn-setting setting__btn">
                    <img
                        class="btn-setting__img"
                        src="img/svg/settings-min.svg"
                        width="16"
                        height="16"
                        alt="Open settings dropdown menu">
                    </button>
                        
                    <ul class="setting__list ${HIDDEN_KEY}">
                        <li class="setting__item" data-action="edit">
                            <button class="setting__action-btn">Edit name</button>
                        </li>
                        <li class="setting__item" data-action="remove">
                            <button class="setting__action-btn">Remove</button>
                        </li>
                    </ul>
                </div>
            </li>`
        );
    }

    /**
     * Choose an action based on the user-selected folder setting button
     *
     * @param {Event} e
     */
    manageFolderSettings(e) {
        const folderDiv = e.target.closest(`.${FOLDER_KEY}`);
        const folderSettingsBtn = e.target.closest(`.${SETTING_BTN_KEY}`);
        const folderActionBtn = e.target.closest(`.${SETTING_ACTION_BTN_KEY}`);

        if (folderSettingsBtn) {
            this.toggleFolderSettings(folderDiv);
        }

        if (folderActionBtn) {
            const folderAction = e.target.parentNode.dataset.action;
            this.runFolderSettingAction(folderDiv, folderAction);
        }
    }

    /**
     * @param {HTMLDivElement} folderDiv
     */
    toggleFolderSettings(folderDiv) {
        const settingList = folderDiv.querySelector('.' + SETTING_LIST_KEY);
        const setting = settingList.parentNode;
        settingList.classList.toggle(HIDDEN_KEY);

        if (setting.classList.contains(SETTING_ACTIVE_KEY)) {
            setting.classList.remove(SETTING_ACTIVE_KEY);
            folderDiv.classList.remove(`${FOLDER_ACTIVE_KEY}`);
        } else {
            this.hideActiveSettings();
            setting.classList.add(SETTING_ACTIVE_KEY);
            folderDiv.classList.add(`${FOLDER_ACTIVE_KEY}`);
        }
    }

    /**
     * @param {HTMLDivElement} folderDiv
     * @param {string} folderAction
     */
    runFolderSettingAction(folderDiv, folderAction) {
        const folderId = folderDiv.dataset.folderId;
        const actions = {
            remove: {
                params: [folderId],
                func: this.removeFolder,
            },
            edit: {
                params: [folderDiv, folderId],
                func: this.showInputForEditFolderName,
            }
        }

        const action = actions[folderAction];
        action.params.push(folderId);
        action.func.apply(this, action.params);
    }

    /**
     * @param {string} folderId
     */
    removeFolder(folderId) {
        (new FoldersResource()).removeFolder(folderId);
        (new NotesView()).curFolder = null;
        this.observer.notify();
    }

    /**
     * @param {HTMLElement} listItem
     * @param {string} folderId
     */
    showInputForEditFolderName(listItem, folderId) {
        const oldFolderTitle = listItem.querySelector(`.${FOLDER_NAME_KEY}`).textContent;
        let item = document.createElement('li');
        let input = document.createElement('input');

        input.classList.add(FOLDER_INPUT_KEY);
        input.value = oldFolderTitle;
        item.classList.add(`${FOLDER_KEY}`);

        item.append(input);
        listItem.after(item);
        input.focus();
        listItem.remove();

        this.folderEditBlurListener(input, folderId);
    }

    /**
     * @param {HTMLInputElement} input
     */
    newFolderBlurListener(input) {
        input.addEventListener('blur', () => {
            let newFolder = new Folder();
            newFolder.title = input.value;

            (new FoldersView()).activeFolderId = newFolder.id;
            (new NotesView()).curFolder = newFolder;
            (new FoldersResource()).addNewFolder(newFolder);

            this.observer.notify();
        });
    }

    /**
     * @param {HTMLInputElement} input
     * @param {string} folderId
     */
    folderEditBlurListener(input, folderId) {
        input.addEventListener('blur', () => {
            (new FoldersResource()).editFolderName(folderId, input.value);
            this.observer.notify();
        });
    }

    /**
     * Hide active folder setting dropdown
     */
    hideActiveSettings() {
        const settingActive = document.querySelector(`.${SETTING_ACTIVE_KEY}`);

        if (!settingActive) {
            return;
        }

        const settingList = settingActive.querySelector(`.${SETTING_LIST_KEY}`);
        const folderItem = document.querySelector(`.${FOLDER_ACTIVE_KEY}`);

        settingActive.classList.remove(SETTING_ACTIVE_KEY);
        settingList.classList.add(HIDDEN_KEY);
        folderItem.classList.remove(`${FOLDER_ACTIVE_KEY}`);
    }

    /**
     * Hide folder active state
     */
    hideFoldersActive() {
        const folders = document.querySelectorAll(`.${FOLDER_KEY}`);
        folders.forEach(folder => {
            folder.classList.remove(`${FOLDER_ACTIVE_KEY}`);
        });
    }
}