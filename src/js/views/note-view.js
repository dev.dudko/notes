class NoteView {
    static noteView = null;

    constructor() {
        if (!NoteView.noteView) {
            NoteView.noteView = this;
            this._observer = null;
            this._curFolder = null;
            this._noteField = null;
        }

        return NoteView.noteView;
    }

    /**
     * @returns {Folder}
     */
    get curFolder() {
        return this._curFolder;
    }

    /**
     * @param {Folder} curFolder
     */
    set curFolder(curFolder) {
        this._curFolder = curFolder;
    }

    /**
     * @returns {HTMLElement}
     */
    get noteField() {
        return this._noteField;
    }

    /**
     * @param {HTMLElement} noteField
     */
    set noteField(noteField) {
        this._noteField = noteField;
    }

    /**
     * @returns {Observer}
     */
    get observer() {
        return this._observer;
    }

    /**
     * @param {Observer} observer
     */
    set observer(observer) {
        this._observer = observer;
    }

    /**
     * @param {string} noteId
     */
    showNote(noteId) {
        let note = this.curFolder.getNoteById(noteId);
        const noteContent = this.noteField.querySelector(`.${NOTE_FIELD_CONTENT_KEY}`);
        noteContent.innerHTML = '';

        let textarea = document.createElement('textarea');
        textarea.classList.add(NOTE_TEXTAREA_KEY);
        textarea.value = note.text;

        noteContent.append(textarea);
        textarea.focus();

        const folderDiv = document.querySelector(`.${FOLDER_ACTIVE_KEY}`);
        const noteDiv = document.querySelector(`.${NOTE_ACTIVE_KEY}`);
        (new FoldersView()).activeFolderId = folderDiv.dataset.folderId;
        (new NotesView()).activeNoteId = noteDiv.dataset.noteId;

        this.textareaInputListener(textarea, note);
    }

    /**
     * Show empty textarea field in the browser view
     */
    addNewNote() {
        let newNote = new Note();
        newNote.text = '';
        newNote.creationDate = Date.now();

        new FoldersResource().addNewNoteInFolder(this.curFolder.id, newNote);
        const noteContent = this.noteField.querySelector(`.${NOTE_FIELD_CONTENT_KEY}`);
        noteContent.innerHTML = '';

        let textarea = document.createElement('textarea');
        textarea.classList.add(NOTE_TEXTAREA_KEY);

        noteContent.append(textarea);
        textarea.placeholder = 'Note...';
        textarea.focus();

        const folderDiv = document.querySelector(`.${FOLDER_ACTIVE_KEY}`);
        (new FoldersView()).activeFolderId = folderDiv.dataset.folderId;
        (new NotesView()).activeNoteId = newNote.id;

        this.textareaInputListener(textarea, newNote);
    }

    /**
     * Listen input changes in the note textarea
     *
     * @param {HTMLTextAreaElement} textarea
     * @param {Note} note
     */
    textareaInputListener(textarea, note) {
        textarea.addEventListener('input', () => {
            note.text = textarea.value;
            this.observer.notify();
            (new FoldersResource()).editNoteInFolder(this.curFolder.title, note);
        });
    }
}