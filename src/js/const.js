const MAX_NOTE_TITLE_LENGTH = 60;
const ELLIPSIS = String.fromCharCode(8230);
const NOTE_DEFAULT_NAME = 'Unnamed';
const FOLDER_DEFAULT_NAME = 'New Folder';

const FOLDER_KEY = 'folder';
const FOLDER_INPUT_KEY = 'folder__input';
const FOLDER_ACTIVE_KEY = 'folder_active';
const FOLDER_NAME_KEY = 'folder__name';
const FOLDERS_ADD_BTN_KEY = 'folder__add-btn';
const FOLDERS_LIST_KEY = 'folders__list';

const SETTING_LIST_KEY = 'setting__list';
const SETTING_BTN_KEY = 'setting__btn';
const SETTING_ACTION_BTN_KEY = 'setting__action-btn';
const SETTING_ACTIVE_KEY = 'setting_active';

const NOTES_KEY = 'notes';
const NOTES_LIST_KEY = 'notes__list';
const NOTES_BTN_KEY = 'notes__btn';
const NOTE_BTN_REMOVE_KEY = 'note__btn-remove';

const NOTE_KEY = 'note';
const NOTE_ACTIVE_KEY = 'note_active';
const NOTE_FIELD_KEY = 'note-field';
const NOTE_ADD_KEY = 'note__add-btn';
const NOTE_FIELD_CONTENT_KEY = 'note-field__content';
const NOTE_TEXTAREA_KEY = 'note__textarea';

const HIDDEN_KEY = '_hidden';