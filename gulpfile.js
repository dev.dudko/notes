const {src, dest, series, watch} = require('gulp');
const babel = require('gulp-babel');
const sass = require('gulp-sass')(require('sass'));
const csso = require('gulp-csso');
const include = require('gulp-file-include');
const htmlmin = require('gulp-htmlmin');
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const sync = require('browser-sync').create();

function html() {
    return src('src/**.html')
        .pipe(include({
            prefix: '@@'
        }))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
        }))
        .pipe(dest('dist'));
}

function scss() {
    return src('src/scss/styles.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            overrideBrowserslist: [
                ">1%",
            ],
        }))
        .pipe(csso())
        .pipe(concat('styles.css'))
        .pipe(dest('dist/css'));
}

function fonts() {
    return src('src/fonts/**/*')
        .pipe(dest('dist/fonts'));
}

function img() {
    return src('src/img/**/*')
        .pipe(dest('dist/img'));
}

function script() {
    return src('src/js/**/*.js')
        .pipe(dest('dist/js'));
}

function clear() {
    return del('dist');
}

function serve() {
    sync.init({
        server: './dist',
        ui: false,
        notify: false,
    });

    watch('src/**/**.html', series(html)).on('change', sync.reload);
    watch('src/scss/**/**.scss', series(scss)).on('change', sync.reload);
    watch('src/js/**/**.js', series(script)).on('change', sync.reload);
    watch('src/fonts/**/*', series(fonts)).on('change', sync.reload);
    watch('src/img/**/*', series(img)).on('change', sync.reload);
}

exports.clear = clear;
exports.build = series(clear, fonts, img, script, scss, html);
exports.default = series(clear, fonts, img, script, scss, html, serve);